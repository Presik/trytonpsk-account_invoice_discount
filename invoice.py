from decimal import Decimal
from trytond.model import fields
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval

from trytond.modules.product import price_digits, round_price

STATES = {
    'invisible': Eval('type') != 'line',
    'required': Eval('type') == 'line',
    }
DEPENDS = ['type']


class InvoiceLine(metaclass=PoolMeta):
    __name__ = 'account.invoice.line'

    base_price = fields.Numeric(
        "Base Price", digits=price_digits,
        states={
            'invisible': Eval('type') != 'line',
            'readonly': Eval('invoice_state') != 'draft',
            },
        depends=['type', 'invoice_state'])

    discount_rate = fields.Function(fields.Numeric(
            "Discount Rate", digits=(16, 4),
            states={
                'invisible': Eval('type') != 'line',
                'readonly': Eval('invoice_state') != 'draft',
                },
            depends=['type', 'invoice_state']),
        'on_change_with_discount_rate', setter='set_discount_rate')
    discount_amount = fields.Function(fields.Numeric(
            "Discount Amount", digits=price_digits,
            states={
                'invisible': Eval('type') != 'line',
                'readonly': Eval('invoice_state') != 'draft',
                },
            depends=['type', 'invoice_state']),
        'on_change_with_discount_amount', setter='set_discount_amount')

    discount = fields.Function(fields.Char(
            "Discount",
            states={
                'invisible': ~Eval('discount'),
                }),
        'on_change_with_discount')

    @fields.depends(
        methods=[
            'compute_base_price', 'on_change_with_discount_rate',
            'on_change_with_discount_amount', 'on_change_with_discount'])
    def on_change_product(self):
        super().on_change_product()
        if self.product:
            self.base_price = self.compute_base_price()
            self.discount_rate = self.on_change_with_discount_rate()
            self.discount_amount = self.on_change_with_discount_amount()
            self.discount = self.on_change_with_discount()

    @fields.depends('product', 'unit')
    def compute_base_price(self):
        pool = Pool()
        Uom = pool.get('product.uom')
        if self.product:
            price = self.product.list_price
            if self.unit:
                price = Uom.compute_price(
                    self.product.default_uom, price, self.unit)
            return round_price(price)

    @fields.depends(
        methods=[
            'compute_base_price', 'on_change_with_discount_rate',
            'on_change_with_discount_amount', 'on_change_with_discount'])
    def on_change_quantity(self):
        self.base_price = self.compute_base_price()
        self.discount_rate = self.on_change_with_discount_rate()
        self.discount_amount = self.on_change_with_discount_amount()
        self.discount = self.on_change_with_discount()

    @fields.depends('unit_price', 'base_price')
    def on_change_with_discount_rate(self, name=None):
        if self.unit_price is None or not self.base_price:
            return
        rate = 1 - self.unit_price / self.base_price
        return rate.quantize(
            Decimal(1) / 10 ** self.__class__.discount_rate.digits[1])

    @fields.depends(
        'base_price', 'discount_rate',
        methods=['on_change_with_discount_amount', 'on_change_with_discount',
            'on_change_with_amount'])
    def on_change_discount_rate(self):
        if self.base_price is not None and self.discount_rate is not None:
            self.unit_price = round_price(
                self.base_price * (1 - self.discount_rate))
            self.discount_amount = self.on_change_with_discount_amount()
            self.discount = self.on_change_with_discount()
            self.amount = self.on_change_with_amount()

    @classmethod
    def set_discount_rate(cls, lines, name, value):
        pass

    @fields.depends('unit_price', 'base_price')
    def on_change_with_discount_amount(self, name=None):
        if self.unit_price is None or self.base_price is None:
            return
        return round_price(self.base_price - self.unit_price)

    @fields.depends(
        'base_price', 'discount_amount',
        methods=['on_change_with_discount_rate', 'on_change_with_discount',
            'on_change_with_amount'])
    def on_change_discount_amount(self):
        if self.base_price is not None and self.discount_amount is not None:
            self.unit_price = round_price(
                self.base_price - self.discount_amount)
            self.discount_rate = self.on_change_with_discount_rate()
            self.discount = self.on_change_with_discount()
            self.amount = self.on_change_with_amount()

    @classmethod
    def set_discount_amount(cls, lines, name, value):
        pass

    @fields.depends(
        'invoice', '_parent_invoice.currency',
        methods=[
            'on_change_with_discount_rate', 'on_change_with_discount_amount'])
    def on_change_with_discount(self, name=None):
        pool = Pool()
        Lang = pool.get('ir.lang')
        lang = Lang.get()
        rate = self.on_change_with_discount_rate()
        if not rate or rate % Decimal('0.01'):
            amount = self.on_change_with_discount_amount()
            if amount:
                return lang.currency(
                    amount, self.invoice.currency, digits=price_digits[1])
        else:
            return lang.format('%i', rate * 100) + '%'


    # gross_unit_price = fields.Numeric('Gross Price', digits=price_digits,
    #     depends=DEPENDS)
    # gross_unit_price_wo_round = fields.Numeric('Gross Price without rounding',
    #     digits=(16, price_digits[1]), readonly=True)
    # discount = fields.Numeric('Discount', digits=(2, 4),
    #     depends=DEPENDS)

    # @classmethod
    # def __setup__(cls):
    #     super(InvoiceLine, cls).__setup__()
    #     cls.unit_price.states['readonly'] = True
    #     cls.unit_price.digits = (20, price_digits[1])
    #     if 'discount' not in cls.amount.on_change_with:
    #         cls.amount.on_change_with.add('discount')
    #     if 'gross_unit_price' not in cls.amount.on_change_with:
    #         cls.amount.on_change_with.add('gross_unit_price')

    # @staticmethod
    # def default_discount():
    #     return Decimal(0)

    # def update_prices(self):
    #     unit_price = self.unit_price
    #     digits = self.__class__.gross_unit_price.digits[1]
    #     gross_unit_price = gross_unit_price_wo_round = self.gross_unit_price

    #     if self.gross_unit_price is not None and self.discount is not None:
    #         unit_price = self.gross_unit_price * (1 - self.discount)
    #         digits = self.__class__.unit_price.digits[1]
    #         unit_price = unit_price.quantize(Decimal(str(10.0 ** -digits)))

    #         if self.discount != 1:
    #             gross_unit_price_wo_round = unit_price / (1 - self.discount)
    #         gross_unit_price = gross_unit_price_wo_round.quantize(
    #             Decimal(str(10.0 ** -digits)))
    #     elif self.unit_price and self.discount:
    #         gross_unit_price_wo_round = self.unit_price / (1 - self.discount)
    #         gross_unit_price = gross_unit_price_wo_round.quantize(
    #             Decimal(str(10.0 ** -digits)))

    #     if gross_unit_price_wo_round:
    #         digits = self.__class__.gross_unit_price_wo_round.digits[1]
    #         gross_unit_price_wo_round = gross_unit_price_wo_round.quantize(
    #             Decimal(str(10.0 ** -digits)))

    #     self.gross_unit_price = gross_unit_price
    #     self.gross_unit_price_wo_round = gross_unit_price_wo_round
    #     self.unit_price = unit_price

    # @fields.depends('gross_unit_price', 'discount', 'unit_price')
    # def on_change_gross_unit_price(self):
    #     return self.update_prices()

    # @fields.depends('gross_unit_price', 'discount', 'unit_price')
    # def on_change_discount(self):
    #     return self.update_prices()

    # @fields.depends('gross_unit_price', 'unit_price', 'discount',
    #     'product', 'unit', 'party', 'invoice', 'invoice_type',
    #     '_parent_invoice.type', '_parent_invoice.party')
    # def on_change_product(self):
    #     super(InvoiceLine, self).on_change_product()
    #     if self.unit_price:
    #         self.gross_unit_price = self.unit_price
    #         self.discount = Decimal(0)
    #         self.update_prices()
    #     if not self.discount:
    #         self.discount = Decimal(0)

    # @classmethod
    # def create(cls, vlist):
    #     vlist = [x.copy() for x in vlist]
    #     for vals in vlist:
    #         if vals.get('type') != 'line':
    #             continue
    #         gross_unit_price = (vals.get('unit_price', Decimal('0.0'))
    #             or Decimal('0.0'))
    #         if 'discount' in vals and vals['discount'] != 1:
    #             gross_unit_price = gross_unit_price / (1 - vals['discount'])
    #             digits = cls.gross_unit_price.digits[1]
    #             gross_unit_price = gross_unit_price.quantize(
    #                 Decimal(str(10.0 ** -digits)))
    #         vals['gross_unit_price'] = gross_unit_price
    #         if 'discount' not in vals:
    #             vals['discount'] = Decimal(0)
    #     return super(InvoiceLine, cls).create(vlist)

    # def _credit(self):
    #     res = super(InvoiceLine, self)._credit()
    #     # FIXME
    #     #for field in ('gross_unit_price', 'discount'):
    #     #    res[field] = getattr(self, field)
    #     return res
