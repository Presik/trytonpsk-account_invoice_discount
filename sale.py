from trytond.pool import PoolMeta


class SaleLine(metaclass=PoolMeta):
    __name__ = 'sale.line'

    def get_invoice_line(self):
        'Return a list of invoice lines for sale line'
        line = super(SaleLine, self).get_invoice_line()
        if line and isinstance(line, list):
            for l in line:
                l.base_price = self.base_price
        return line
